#!/bin/sh

touch /traefik/acme.json && chmod 600 /traefik/acme.json

cp /traefik/traefik.toml /traefik.toml

[ -z "$EMAIL" ] && \
  echo "EMAIL env variable is missing. Stating Traefik in DEVELOPMENT mode. Using local certificates" && \
  traefik --api --docker

printf "\nemail = \"$EMAIL\"\n" >> /traefik.toml

if [ -z "$WILDCARD_DOMAINS" ]
then
  printf "onHostRule = true\n" >> /traefik.toml
  printf "\n[acme.httpChallenge]\n  entryPoint = \"http\"\n" >> /traefik.toml
else
  [ -z "$PROVIDER_CODE" ] && echo "PROVIDER_CODE is missing. Please double check .env" && exit 1
  [ -z "$WILDCARD_DOMAINS" ] && echo "WILDCARD_DOMAINS field is empty. Please double check .env" && exit 1

  printf "\n[acme.dnsChallenge]\n  provider = \"$PROVIDER_CODE\"\n" >> /traefik.toml

  for domain in ${WILDCARD_DOMAINS//,/ }
  do
    printf "\n[[acme.domains]]\n  main = \"*.$domain\"\n  sans = [\"$domain\"]\n" >> /traefik.toml
  done
fi

traefik --configfile=/traefik.toml --api --docker
